function EnviarMensagem(){
    //se a mensagem nao for vazia
    if(txt_msg.value !== ""){
        //texto da caixa se torna a mensagem digitada 
        novo_txt.innerText = txt_msg.value;
        //adiciona o texto
        msg_si.append(novo_txt);
        txt_msg.value = ""
    }else{
        alert("digite uma mensagem")
    }
}

function ExcluirMensagem(){
    //limpando a caixa de texto
    msg_si.innerHTML = "";
}

function EditarMensagem(){
    //editando para um anova mensagem digitada
    novo_txt.innerText = txt_msg.value
}

//elementos html
const btn_enviar = document.querySelector("#btn_enviar");
const btn_editar = document.querySelector("#btn_editar");
const btn_excluir = document.querySelector("#btn_excluir");
const msg_si = document.querySelector(".mensagem_si");
const txt_msg = document.querySelector("#txt_mensagem");

//mensagem
let novo_txt = document.createElement("p");

//botoes chamam as funções
btn_enviar.addEventListener("click", ()=>{EnviarMensagem()});
btn_excluir.addEventListener("click", ()=>{ExcluirMensagem()});
btn_editar.addEventListener("click", ()=>{EditarMensagem()})